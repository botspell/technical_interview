import _ from 'lodash'
import { makeStyles } from '@material-ui/core/styles'
import { Container } from '@material-ui/core'
// import SearchBar from 'components/SearchBar'
// import { defaultUsers } from 'lib/constants'

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		width: '100%',
		height: '100%',
		padding: '20px',
		backgroundColor: '#f5f5f5',
		'& > *': {
			margin: theme.spacing(2, 0)
		}
	}
}))

export default function Home () {
	const classes = useStyles()

	return (
		<Container className={classes.root}>

			{/* Finish implementing SearchBar */}

			{/* Display Users */}

		</Container>
	)
}

function User ({ first_name, last_name }) {
	return (
		<div>
			<span>{first_name}</span>
			<span>{last_name}</span>
		</div>
	)
}

